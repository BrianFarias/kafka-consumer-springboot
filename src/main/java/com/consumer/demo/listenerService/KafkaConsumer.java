package com.consumer.demo.listenerService;

import com.consumer.demo.dto.UserDto;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

//    @KafkaListener(topics = "Kafka_Example", groupId = "group_id")
//    public void consume(String message){
//        System.out.println("Consumed Message: " + message.toString());
//    }

    @KafkaListener(topics = "Kafka_Example", groupId = "group_id")
    public void consumeJson(UserDto user){
        System.out.println("Consumed Json Message: " + user.toString());
    }

}
